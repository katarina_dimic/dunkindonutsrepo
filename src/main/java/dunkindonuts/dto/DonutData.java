package dunkindonuts.dto;

import dunkindonuts.entities.Donut;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

public class DonutData implements Serializable {

    private long id;

    private String name;

    private double price;

    private ImageData image;

    public DonutData(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.price = builder.price;
        this.image = builder.image;
    }

    public DonutData() {
        //required by Jackson
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public ImageData getImage() {
        return image;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("name", name).append("price", price).build();
    }

    public static Builder builder(){
        return new Builder();
    }

    public static Builder builder(Donut donut){
        return new Builder(donut);
    }

    public static final class Builder {
        private long id;
        private String name;
        private double price;
        private ImageData image;

        public Builder() {
        }

        public Builder(Donut donut) {
            this.id = donut.getId();
            this.name = donut.getName();
            this.price = donut.getPrice();
//            this.image = ImageData.builder(donut.getImage()).build();
        }

        public Builder id(long id){
            this.id = id;
            return this;
        }

        public Builder name(String name){
            this.name = name;
            return this;
        }

        public Builder price(double price){
            this.price = price;
            return this;
        }

        public Builder image(ImageData image){
            this.image = image;
            return this;
        }

        public DonutData build(){
            return new DonutData(this);
        }
    }
}
