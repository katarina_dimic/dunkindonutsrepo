package dunkindonuts.dto;

import dunkindonuts.entities.Donut;
import dunkindonuts.entities.Image;

public class ImageData {

    private Long id;

    private String name;

    private byte[] content;

    public ImageData(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.content = builder.content;
    }

    public ImageData() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public byte[] getContent() {
        return content;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static Builder builder(Image image){
        return new Builder(image);
    }

    public static final class Builder {
        private long id;
        private String name;
        private byte[] content;

        public Builder() {
        }

        public Builder(Image image) {
            this.id = image.getId();
            this.name = image.getName();
            this.content = image.getContent();
        }

        public Builder id(long id){
            this.id = id;
            return this;
        }

        public Builder name(String name){
            this.name = name;
            return this;
        }

        public Builder content(byte[] content){
            this.content = content;
            return this;
        }

        public ImageData build(){
            return new ImageData(this);
        }
    }
}
