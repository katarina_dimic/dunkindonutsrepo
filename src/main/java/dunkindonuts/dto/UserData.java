package dunkindonuts.dto;

import dunkindonuts.entities.User;
import dunkindonuts.entities.UserType;
import dunkindonuts.validation.ValidRegistration;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@ValidRegistration
public class UserData implements Serializable {

    private long id;
    @NotBlank(message = "This field cannot be empty")
    private String firstname;
    @NotBlank(message = "This field cannot be empty")
    private String lastname;
    @NotBlank(message = "This field cannot be empty")
    private String username;
    @Size(min = 8)
    private String password;
    private UserType type;

    public UserData() {
        // required by Jackson
    }

    public UserData(Builder builder) {
        this.id = builder.id;
        this.firstname = builder.firstname;
        this.lastname = builder.lastname;
        this.username = builder.username;
        this.password = builder.password;
        this.type = builder.type;
    }

    public long getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public UserType getType() {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id)
                .append("firstname", firstname).append("lastname", lastname).append("username", username)
                .append("password", password).append("type", type.value()).build();
    }

    public static Builder builder(){
        return new Builder();
    }

    public static Builder builder(User user){
        return new Builder(user);
    }

    public static final class Builder {
        private long id;
        private String firstname;
        private String lastname;
        private String username;
        private String password;
        private UserType type;

        public Builder() {}

        public Builder(User user) {
            this.id = user.getId();
            this.firstname = user.getFirstname();
            this.lastname = user.getLastname();
            this.username = user.getUsername();
            this.password = user.getPassword();
            this.type = user.getType();
        }

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder firstname(String firstname) {
            this.firstname = firstname;
            return this;
        }

        public Builder lastname(String lastname) {
            this.lastname = lastname;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder type(UserType userType) {
            this.type = userType;
            return this;
        }

        public UserData build() {
            return new UserData(this);
        }

    }

}
