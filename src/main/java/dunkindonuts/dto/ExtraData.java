package dunkindonuts.dto;

import dunkindonuts.entities.Donut;
import dunkindonuts.entities.Extra;

public class ExtraData {

    private long id;

    private String name;

    public ExtraData() {
    }

    public ExtraData(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static Builder builder(Extra extra){
        return new Builder(extra);
    }

    public static final class Builder {
        private long id;
        private String name;

        public Builder() {
        }

        public Builder(Extra extra) {
            this.id = extra.getId();
            this.name = extra.getName();
        }

        public Builder id(long id){
            this.id = id;
            return this;
        }

        public Builder name(String name){
            this.name = name;
            return this;
        }

        public ExtraData build(){
            return new ExtraData(this);
        }
    }
}
