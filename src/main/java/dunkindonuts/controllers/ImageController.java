package dunkindonuts.controllers;

import dunkindonuts.dto.ImageData;
import dunkindonuts.services.management.ImageManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(path = "/image")
public class ImageController {

    @Autowired
    private ImageManagementService imageManagementService;

    @RequestMapping(path = "/add", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> addImage(@RequestParam(required = true, value = "file") MultipartFile file) throws Exception {
        ImageData imageData = ImageData.builder().name(file.getOriginalFilename()).content(file.getBytes()).build();
        imageManagementService.addImage(imageData);

        return ResponseEntity.ok("Image added succesfully to the database");
    }

    @GetMapping(path = "/{name}/get")
    public ImageData retrieveDonut(@PathVariable String name) {
        return imageManagementService.getImage(name);
    }

}
