package dunkindonuts.controllers;

import dunkindonuts.dto.DonutData;
import dunkindonuts.dto.ExtraData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import dunkindonuts.services.management.DonutManagementService;

import java.util.List;

@RestController
@RequestMapping(path = "/donut")
public class DonutController {

    @Autowired
    private DonutManagementService donutManagementService;

    @PostMapping(path = "/add")
    public void addDonut(@RequestBody DonutData donutData) {
        donutManagementService.addDonut(donutData);
    }

    @GetMapping(path = "/{name}/get")
    public DonutData retrieveDonut(@PathVariable String name) {
        return donutManagementService.getDonut(name);
    }

    @GetMapping(path = "/getAll")
    public List<DonutData> retrieveAllDonuts() {
        return donutManagementService.getAllDonuts();
    }

    @GetMapping(path = "/{name}/getExtras")
    public List<ExtraData> retrieveExtrasForDonut(@PathVariable String name) {
        return donutManagementService.getAllExtrasForDonut(name);
    }

}
