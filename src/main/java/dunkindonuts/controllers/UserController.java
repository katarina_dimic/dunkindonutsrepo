package dunkindonuts.controllers;

import dunkindonuts.dto.UserData;
import dunkindonuts.entities.UserType;
import dunkindonuts.services.management.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserManagementService userManagementService;

    @PostMapping("/registration")
    public void addUser(@RequestBody UserData userData) {
        userManagementService.addUser(userData);
    }

    @GetMapping("/{type}")
    public List<UserData> retrieveUsersByType(@PathVariable String type) {
        return userManagementService.getByType(UserType.valueOf(type));
    }
}
