package dunkindonuts.services;

import dunkindonuts.entities.Donut;
import dunkindonuts.entities.User;
import dunkindonuts.entities.UserType;

import java.util.List;

public interface UserService {
    /**
     * Fetches {@link User} by given username.
     *
     * @param username - the name of the {@link User} which should be fetched.
     * @return
     */
    User findByUsername(String username);

    /**
     * Fetches list of {@link User} by given {@link dunkindonuts.entities.UserType}.
     *
     * @param type - {@link dunkindonuts.entities.UserType} of the {@link User}s which should be fetched.
     * @return list of {@link User} or throw exception
     */
    List<User> findByType(UserType type);

    /**
     * Inserts new user to the database.
     *
     * @param user - donut to be inserted.
     */
    void insert(User user);
}
