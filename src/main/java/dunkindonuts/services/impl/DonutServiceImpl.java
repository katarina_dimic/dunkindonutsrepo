package dunkindonuts.services.impl;

import dunkindonuts.dao.DonutDao;
import dunkindonuts.entities.Donut;
import dunkindonuts.entities.Extra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import dunkindonuts.services.DonutService;

import java.util.List;

@Service
@Transactional
public class DonutServiceImpl implements DonutService {

    @Autowired
    private DonutDao donutDao;

    @Override
    public Donut findByName(String name) {
        return donutDao.findByName(name).orElseThrow(() -> new RuntimeException(String.format("No donut found with name %s", name)));
    }

    @Override
    public double findPriceById(long id) {
        return donutDao.findPriceById(id);
    }

    @Override
    public void insert(Donut donut){
        if (donut != null) {
            donutDao.insert(donut);
        } else {
            throw new RuntimeException("Cannot insert null value");
        }
    }

    @Override
    public void delete(Donut donut) {
        donutDao.delete(donut);
    }

    @Override
    public List<Donut> getAll() {
        return donutDao.getAll();
    }

    @Override
    public List<Extra> getAllExtrasForDonut(String name) {
        Donut donut = findByName(name);
        return donutDao.getAllExtrasForDonut(donut.getId());
    }
}
