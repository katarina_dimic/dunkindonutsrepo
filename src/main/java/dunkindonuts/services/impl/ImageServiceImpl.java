package dunkindonuts.services.impl;

import dunkindonuts.dao.ImageDao;
import dunkindonuts.entities.Image;
import dunkindonuts.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ImageServiceImpl implements ImageService {

    @Autowired
    private ImageDao imageDao;

    @Override
    public Image findByName(String name) {
        return imageDao.findByName(name).orElseThrow(() -> new RuntimeException(String.format("No image found with name %s", name)));
    }

    @Override
    public void insert(Image image) {
        imageDao.insert(image);
    }
}
