package dunkindonuts.services.impl;

import dunkindonuts.dao.UserDao;
import dunkindonuts.entities.User;
import dunkindonuts.entities.UserType;
import dunkindonuts.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username).orElseThrow(() -> new RuntimeException(String.format("No user found with username %s", username)));
    }

    @Override
    public List<User> findByType(UserType type) {
        List<User> users = userDao.findByType(type);
        if (users.isEmpty()) {
            throw new RuntimeException(String.format("Users with type: %s are not found", type.value()));
        }
        return users;
    }

    @Override
    public void insert(User user) {
        if (user != null) {
            userDao.insert(user);
        } else {
            throw new RuntimeException("Cannot insert null value");
        }
    }
}
