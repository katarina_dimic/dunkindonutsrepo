package dunkindonuts.services;

import dunkindonuts.entities.Donut;
import dunkindonuts.entities.Extra;

import java.util.List;

public interface DonutService {
    /**
     * Fetches {@link Donut} by given name.
     *
     * @param name - the name of the {@link Donut} which should be fetched.
     * @return
     */
    Donut findByName(String name);

    /**
     * Fetches the price of the {@link Donut} by given id.
     *
     * @param id - the id od the {@link Donut} whose price should be fetched.
     * @return
     */
    double findPriceById(long id);

    /**
     * Inserts new donut to the database.
     *
     * @param donut - donut to be inserted.
     */
    void insert(Donut donut);

    /**
     * Delete existing donut from the database.
     *
     * @param donut - donut to be deleted.
     */
    void delete(Donut donut);

    /**
     * Fetches all {@link Donut}s in the system.
     *
     * @return
     */
    List<Donut> getAll();

    /**
     * Fetch all {@link Extra}s for {@link Donut} specified by given id.
     *
     * @param name
     * @return
     */
    List<Extra> getAllExtrasForDonut(String name);
}
