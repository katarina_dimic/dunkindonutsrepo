package dunkindonuts.services;

import dunkindonuts.entities.Image;

public interface ImageService {
    /**
     * Fetches {@link Image} by given name.
     *
     * @param name - the name of the {@link Image} which should be fetched.
     * @return
     */
    Image findByName(String name);

    /**
     * Inserts new image to the database.
     *
     * @param image - image to be inserted.
     */
    void insert(Image image);
}
