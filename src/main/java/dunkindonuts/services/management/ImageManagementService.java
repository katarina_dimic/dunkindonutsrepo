package dunkindonuts.services.management;

import dunkindonuts.dto.DonutData;
import dunkindonuts.dto.ImageData;

public interface ImageManagementService {

    /**
     *
     * @param name
     * @return
     */
    ImageData getImage(String name);

    /**
     *
     * @param imageData
     */
    void addImage(ImageData imageData);
}
