package dunkindonuts.services.management.impl;

import dunkindonuts.dto.DonutData;
import dunkindonuts.dto.ExtraData;
import dunkindonuts.dto.ImageData;
import dunkindonuts.entities.Donut;
import dunkindonuts.entities.Extra;
import dunkindonuts.entities.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import dunkindonuts.services.DonutService;
import dunkindonuts.services.management.DonutManagementService;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DonutManagementServiceImpl implements DonutManagementService {

    @Autowired
    private DonutService donutService;

    @Override
    public DonutData getDonut(String name) {
        return DonutData.builder(donutService.findByName(name)).build();
    }

    @Override
    public void addDonut(DonutData donutData) {
        donutService.insert(fill(donutData));
    }

    @Override
    public List<DonutData> getAllDonuts() {
        List<DonutData> result = new ArrayList<>();
        List<Donut> donuts = donutService.getAll();
        donuts.forEach( (donut) -> result.add(DonutData.builder(donut).build()) );
        return result;
    }

    @Override
    public List<ExtraData> getAllExtrasForDonut(String name) {
        List<ExtraData> result = new ArrayList<>();
        List<Extra> extras = donutService.getAllExtrasForDonut(name);
        extras.forEach( (extra) -> result.add(ExtraData.builder(extra).build()) );
        return result;
    }

    private Donut fill(DonutData donutData) {
        Image image = Image.builder().content(donutData.getImage().getContent()).name(donutData.getImage().getName()).build();
        return Donut.builder().name(donutData.getName()).image(image).price(donutData.getPrice()).build();
    }
}
