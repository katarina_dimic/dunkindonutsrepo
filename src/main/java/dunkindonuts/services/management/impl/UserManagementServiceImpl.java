package dunkindonuts.services.management.impl;

import dunkindonuts.dto.UserData;
import dunkindonuts.entities.User;
import dunkindonuts.entities.UserType;
import dunkindonuts.services.UserService;
import dunkindonuts.services.management.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserManagementServiceImpl implements UserManagementService {

    @Autowired
    private UserService userService;

    @Override
    public List<UserData> getByType(UserType userType) {
        return userService.findByType(userType).stream().map(user -> UserData.builder(user).build()).collect(Collectors.toList());
    }

    @Override
    public void addUser(UserData userData) {
        userService.insert(createUser(userData));
    }

    private User createUser(UserData userData) {
        return User.builder().id(userData.getId()).firstname(userData.getFirstname()).lastname(userData.getLastname())
                .username(userData.getUsername()).password(userData.getPassword()).type(userData.getType()).build();
    }
}
