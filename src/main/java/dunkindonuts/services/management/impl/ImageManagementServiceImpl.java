package dunkindonuts.services.management.impl;

import dunkindonuts.dto.ImageData;
import dunkindonuts.entities.Image;
import dunkindonuts.services.ImageService;
import dunkindonuts.services.management.ImageManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ImageManagementServiceImpl implements ImageManagementService {

    @Autowired
    private ImageService imageService;

    @Override
    public ImageData getImage(String name) {
        return ImageData.builder(imageService.findByName(name)).build();
    }

    @Override
    public void addImage(ImageData imageData) {
        imageService.insert(fill(imageData));
    }

    private Image fill(ImageData imageData) {
        return Image.builder().name(imageData.getName()).content(imageData.getContent()).build();
    }
}
