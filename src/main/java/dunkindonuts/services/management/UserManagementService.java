package dunkindonuts.services.management;

import dunkindonuts.dto.UserData;
import dunkindonuts.entities.User;
import dunkindonuts.entities.UserType;

import java.util.List;

public interface UserManagementService {

    /**
     * Retrieves {@link User}s with given {@link UserType} wrapped in {@link UserData}
     * @param userType
     * @return
     */
    List<UserData> getByType(UserType userType);

    /**
     *
     * @param userData
     */
    void addUser(UserData userData);
}
