package dunkindonuts.services.management;

import dunkindonuts.dto.DonutData;
import dunkindonuts.dto.ExtraData;

import java.util.List;

public interface DonutManagementService {

    /**
     *
     * @param name
     * @return
     */
    DonutData getDonut(String name);

    /**
     *
     * @param donutData
     */
    void addDonut(DonutData donutData);

    /**
     *
     * @return
     */
    List<DonutData> getAllDonuts();

    /**
     *
     * @param name
     * @return
     */
    List<ExtraData> getAllExtrasForDonut(String name);
}
