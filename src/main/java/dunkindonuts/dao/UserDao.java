package dunkindonuts.dao;

import dunkindonuts.entities.Donut;
import dunkindonuts.entities.User;
import dunkindonuts.entities.UserType;

import java.util.List;
import java.util.Optional;

public interface UserDao {
    /**
     * Fetches {@link User} by given username.
     *
     * @param username - the username of the {@link User} which should be fetched.
     * @return
     */
    Optional<User> findByUsername(String username);

    /**
     * Fetches list of {@link User} by given {@link dunkindonuts.entities.UserType}.
     *
     * @param type - {@link dunkindonuts.entities.UserType} of the {@link User}s which should be fetched.
     * @return list of {@link User}
     */
    List<User> findByType(UserType type);

    /**
     * Inserts new user to the database.
     *
     * @param user - user to be inserted.
     */
    void insert(User user);
}
