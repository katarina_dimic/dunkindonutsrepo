package dunkindonuts.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

public abstract class AbstractDao<T extends Serializable> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @PersistenceContext
    protected EntityManager entityManager;

    public Optional<T> findById(Class<T> entityType, Long id){
        return id != null ? Optional.ofNullable(entityManager.find(entityType, id)) : Optional.empty();
    }

    public void create(T entity){
        log.debug("Creating entity {}", entity);
        entityManager.persist(entity);
    }

    public T update(T entity){
        log.debug("Updating entity {}", entity);
        return entityManager.merge(entity);
    }

    public void delete(T entity){
        log.debug("Deleting entity {}", entity);
        entityManager.remove( entity );
    }

    @SuppressWarnings("unchecked")
    protected Optional<T> findOptionalResult(Query query) {
        try {
            return Optional.of((T)query.getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }
}
