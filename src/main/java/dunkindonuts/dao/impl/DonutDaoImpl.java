package dunkindonuts.dao.impl;

import dunkindonuts.dao.AbstractDao;
import dunkindonuts.dao.DonutDao;
import dunkindonuts.entities.Donut;
import dunkindonuts.entities.Extra;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Repository
@MappedSuperclass
@Transactional(propagation = Propagation.MANDATORY)
@NamedQueries({
        @NamedQuery(name = "Donut.findByName", query = "SELECT d FROM Donut d where d.name = :name"),
        @NamedQuery(name = "Donut.findPriceById", query = "SELECT d.price FROM Donut d where d.id = :id"),
        @NamedQuery(name = "Donut.getAll", query = "SELECT d FROM Donut d"),
        @NamedQuery(name = "Donut.getAllExtrasForDonut", query = "SELECT e FROM Extra e INNER JOIN e.donuts donut WHERE donut.id = :id")
})
public class DonutDaoImpl extends AbstractDao<Donut> implements DonutDao {

    @Override
    public Optional<Donut> findByName(String name) {
        TypedQuery<Donut> query = entityManager.createNamedQuery("Donut.findByName", Donut.class);
        query.setParameter("name", name);
        return findOptionalResult(query);
    }

    @Override
    public double findPriceById(long id) {
        TypedQuery<Double> query = entityManager.createNamedQuery("Donut.findPriceById", Double.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    @Override
    public void insert(Donut donut) {
        super.create(donut);
    }

    @Override
    public void delete(Donut donut) {
        super.delete(donut);
    }

    @Override
    public List<Donut> getAll() {
        TypedQuery<Donut> query = entityManager.createNamedQuery("Donut.getAll", Donut.class);
        return query.getResultList();
    }

    @Override
    public List<Extra> getAllExtrasForDonut(long id) {
        TypedQuery<Extra> query = entityManager.createNamedQuery("Donut.getAllExtrasForDonut", Extra.class);
        query.setParameter("id", id);
        return query.getResultList();
    }
}
