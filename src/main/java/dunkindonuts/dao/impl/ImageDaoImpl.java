package dunkindonuts.dao.impl;

import dunkindonuts.dao.AbstractDao;
import dunkindonuts.dao.ImageDao;
import dunkindonuts.entities.Image;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.MappedSuperclass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Repository
@MappedSuperclass
@Transactional(propagation = Propagation.MANDATORY)
@NamedQueries({
        @NamedQuery(name = "Image.findByName", query = "SELECT i FROM Image i where i.name = :name")
})
public class ImageDaoImpl extends AbstractDao<Image> implements ImageDao {
    @Override
    public Optional<Image> findByName(String name) {
        TypedQuery<Image> query = entityManager.createNamedQuery("Image.findByName", Image.class);
        query.setParameter("name", name);
        return findOptionalResult(query);
    }

    @Override
    public void insert(Image image) {
        super.create(image);
    }
}
