package dunkindonuts.dao.impl;

import dunkindonuts.dao.AbstractDao;
import dunkindonuts.dao.UserDao;
import dunkindonuts.entities.Donut;
import dunkindonuts.entities.User;
import dunkindonuts.entities.UserType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.MappedSuperclass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Repository
@MappedSuperclass
@Transactional(propagation = Propagation.MANDATORY)
@NamedQueries({
        @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u where u.username = :username"),
        @NamedQuery(name = "User.findByType", query = "SELECT u FROM User u where u.type = :type")
})
public class UserDaoImpl extends AbstractDao<User> implements UserDao {

    @Override
    public Optional<User> findByUsername(String username) {
        TypedQuery<User> query = entityManager.createNamedQuery("User.findByUsername", User.class);
        query.setParameter("username", username);
        return findOptionalResult(query);
    }

    @Override
    public List<User> findByType(UserType type) {
        TypedQuery<User> query = entityManager.createNamedQuery("User.findByType", User.class);
        query.setParameter("type", type.value());
        return query.getResultList();
    }

    @Override
    public void insert(User user) {
        super.create(user);
    }
}
