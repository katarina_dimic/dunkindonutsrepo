package dunkindonuts.dao;

import dunkindonuts.entities.Image;

import java.util.Optional;

public interface ImageDao {
    /**
     * Fetches {@link Image} by given name.
     *
     * @param name - the name of the {@link Image} which should be fetched.
     * @return
     */
    Optional<Image> findByName(String name);

    /**
     * Inserts new image to the database.
     *
     * @param image - image to be inserted.
     */
    void insert(Image image);
}
