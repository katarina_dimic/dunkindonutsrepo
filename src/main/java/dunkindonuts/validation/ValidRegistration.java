package dunkindonuts.validation;

import dunkindonuts.validation.validators.UserRegistrationValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {UserRegistrationValidator.class})
public @interface ValidRegistration {
    String message() default "Invalid username, already exists";
    Class<?>[] groups() default {};
    Class <? extends Payload>[] payload() default {};
}
