package dunkindonuts.validation.validators;

import dunkindonuts.dao.UserDao;
import dunkindonuts.dto.UserData;
import dunkindonuts.entities.User;
import dunkindonuts.services.UserService;
import dunkindonuts.validation.ValidRegistration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

@Component
public class UserRegistrationValidator implements ConstraintValidator<ValidRegistration, UserData> {

    @Autowired
    UserDao userDao;

    @Override
    public boolean isValid(UserData userData, ConstraintValidatorContext constraintValidatorContext) {
        Optional<User> user = userDao.findByUsername(userData.getUsername());
        return !user.isPresent();
    }
}
