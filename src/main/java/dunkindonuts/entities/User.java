package dunkindonuts.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_table")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private UserType type;

    @OneToOne(mappedBy="buyer")
    private Order order1;

    @OneToOne(mappedBy="seller")
    private Order order2;

    public User() {
        //empty construcotr
    }

    public User(Builder builder) {
        this.id = builder.id;
        this.lastname = builder.lastname;
        this.firstname = builder.firstname;
        this.username = builder.username;
        this.password = builder.password;
        this.type = builder.type;
    }

    public long getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public UserType getType() {
        return type;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static final class Builder {
        private long id;
        private String firstname;
        private String lastname;
        private String username;
        private String password;
        private UserType type;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder firstname(String firstname) {
            this.firstname = firstname;
            return this;
        }

        public Builder lastname(String lastname) {
            this.lastname = lastname;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder type(UserType type) {
            this.type = type;
            return this;
        }

        public User build(){
            return new User(this);
        }
    }
}
