package dunkindonuts.entities;

public enum UserType {
    SALESMAN,
    BUYER;

    public String value() {
        return name();
    }

}
