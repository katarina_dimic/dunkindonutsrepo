package dunkindonuts.entities;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "order_table")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "date")
    private ZonedDateTime date;

    @Column(name = "price")
    private double price;

    @Column(name = "accepted")
    private boolean accepted;

    @OneToOne
    private User buyer;

    @OneToOne
    private User seller;

    @OneToMany(mappedBy = "order")
    private List<OrderItem> orderItems = new ArrayList<>();

    public Order() {
        //empty constructor
    }

    public Order(Builder builder) {
        this.id = builder.id;
        this.date = builder.date;
        this.price = builder.price;
        this.accepted = builder.accepted;
    }

    public long getId() {
        return id;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public double getPrice() {
        return price;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static final class Builder{
        private long id;
        private ZonedDateTime date;
        private double price;
        private boolean accepted;

        public Builder id(long id){
            this.id = id;
            return this;
        }

        public Builder date(ZonedDateTime date){
            this.date = date;
            return this;
        }

        public Builder price(double price){
            this.price = price;
            return this;
        }

        public Builder accepted(boolean accepted){
            this.accepted = accepted;
            return this;
        }

        public Order build(){
            return new Order(this);
        }
    }
}
