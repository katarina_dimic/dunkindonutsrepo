package dunkindonuts.entities;

import javax.persistence.*;

@Entity
@Table(name = "order_item_table")
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name="idOrder")
    private Order order;

    @OneToOne
    private Donut donut;

    @OneToOne
    private Extra extra1;

    @OneToOne
    private Extra extra2;

    public OrderItem() {
        //empty constructor
    }

    public OrderItem(Builder builder) {
        this.id = builder.id;
    }

    public long getId() {
        return id;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static final class Builder{
        private long id;

        public Builder id(int id){
            this.id = id;
            return this;
        }

        public OrderItem build(){
            return new OrderItem(this);
        }
    }
}
