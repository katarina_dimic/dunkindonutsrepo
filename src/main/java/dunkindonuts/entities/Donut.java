package dunkindonuts.entities;

import dunkindonuts.dto.DonutData;
import dunkindonuts.dto.ImageData;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "donut_table")
public class Donut implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private double price;

    @OneToOne(mappedBy="donut")
    private OrderItem orderItem;

    @OneToOne
    private Image image;

    @ManyToMany
    @JoinTable(name="rel_donut_extra_table",
            joinColumns=@JoinColumn(name="idDonut"),
            inverseJoinColumns=@JoinColumn(name="idExtra"))
    private Set<Extra> extras = new HashSet<>();

    public Donut() {
        //empty constructor
    }

    public Donut(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.price = builder.price;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public Image getImage() { return image; }

    public static Builder builder(){
        return new Builder();
    }

    public static final class Builder {
        private long id;
        private String name;
        private double price;
        private Image image;

        public Builder id(long id){
            this.id = id;
            return this;
        }

        public Builder name(String name){
            this.name = name;
            return this;
        }

        public Builder price(double price){
            this.price = price;
            return this;
        }

        public Builder image(Image image) {
            this.image = image;
            return this;
        }

        public Donut build(){
            return new Donut(this);
        }
    }
}
