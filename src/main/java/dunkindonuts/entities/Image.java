package dunkindonuts.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "image_table")
public class Image implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Lob
    @Column(name = "content")
    private byte[] content;

    @Column(name = "name")
    private String name;

    @OneToOne(mappedBy = "image")
    private Donut donut;

    public Image() {
    }

    public Image(Builder builder) {
        this.id = builder.id;
        this.content = builder.content;
        this.name = builder.name;
    }

    public Long getId() {
        return id;
    }

    public byte[] getContent() {
        return content;
    }

    public String getName() {
        return name;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static final class Builder {
        private Long id;
        private String name;
        private byte[] content;

        public Builder id(long id){
            this.id = id;
            return this;
        }

        public Builder name(String name){
            this.name = name;
            return this;
        }

        public Builder content(byte[] content){
            this.content = content;
            return this;
        }

        public Image build(){
            return new Image(this);
        }
    }
}
