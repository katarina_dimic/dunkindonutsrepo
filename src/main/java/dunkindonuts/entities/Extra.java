package dunkindonuts.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "extra_table")
public class Extra {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "extras")
    private Set<Donut> donuts = new HashSet<>();

    @OneToOne(mappedBy = "extra1")
    private OrderItem orderItem1;

    @OneToOne(mappedBy = "extra2")
    private OrderItem orderItem2;

    public Extra() {
        //empty constructor
    }

    public Extra(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static final class Builder{
        private long id;
        private String name;

        public Builder id(long id){
            this.id = id;
            return this;
        }

        public Builder name(String name){
            this.name = name;
            return this;
        }

        public Extra build(){
            return new Extra(this);
        }
    }
}
